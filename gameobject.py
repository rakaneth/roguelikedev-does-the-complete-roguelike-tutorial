import tcod

class Entity:
  def __init__(self, glyph='@', name='No Name', color=tcod.white):
    self.glyph = glyph
    self.name = name
    self.color = color
    self.x = 0
    self.y = 0
  
  def set_pos(self, x, y):
    self.x = x
    self.y = y
  
