import tcod as libtcod

from gameobject import Entity

def main():
  screen_width = 80
  screen_height = 50
  #cursor_x, cursor_y = 0, 0
  player = Entity(color=libtcod.yellow)

  libtcod.console_set_custom_font('dejavu16x16_gs_tc.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)
  libtcod.console_init_root(screen_width, screen_height, 'RL Tutorial 2018', False)
  con = libtcod.console_new(screen_width, screen_height)
  libtcod.console_set_default_foreground(con, libtcod.white)

  while True:
    libtcod.console_clear(con)
    libtcod.console_put_char_ex(con, player.x, player.y, player.glyph, player.color, libtcod.black)
    libtcod.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0)
    libtcod.console_flush()
    key = libtcod.console_wait_for_keypress(False)
    action = handle_keys(key)
    move = action.get('move')
    done = action.get('exit')

    if action.get('move'):
      dx, dy = move
      player.set_pos(player.x + dx, player.y + dy)
    
    if done:
      break
  
  raise SystemExit
  

def handle_keys(key):

  moves = {
    libtcod.KEY_UP: (0, -1),
    libtcod.KEY_KP8: (0, -1),
    libtcod.KEY_KP9: (1, -1),
    libtcod.KEY_RIGHT: (1, 0),
    libtcod.KEY_KP6: (1, 0),
    libtcod.KEY_KP3: (1, 1),
    libtcod.KEY_DOWN: (0, 1),
    libtcod.KEY_KP2: (0, 1),
    libtcod.KEY_KP1: (-1, 1),
    libtcod.KEY_LEFT: (-1, 0),
    libtcod.KEY_KP4: (-1, 0),
    libtcod.KEY_KP7: (-1, -1)
  }

  move = moves.get(key.vk)

  if move:
    return {'move': move}
  elif key.vk == libtcod.KEY_ESCAPE:
    return {'exit': True}
  else:
    return {}
    

if __name__ == '__main__':
  main()